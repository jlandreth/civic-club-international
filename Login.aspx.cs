﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class Login : System.Web.UI.Page
{
    int user_access_id;
    protected void Page_Load(object sender, EventArgs e)
    {
        //MessageBox.Show("opening page");

    }
    protected void LoginBtn_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CivicClubInternationalConnectionString"].ConnectionString);
        
        con.Open();
        SqlCommand cmd = new SqlCommand("select username,  password, userType FROM jason.Users WHERE username=@username and password=@password", con);
        cmd.Parameters.AddWithValue("@username", userTxt.Text);
        cmd.Parameters.AddWithValue("@password", passTxt.Text);
        SqlDataReader myReader = null;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
       // myReader = cmd.ExecuteReader();
        String user_id;
        
        foreach (DataRow dr in dt.Rows)
        {
                    Label1.Text = dr[0].ToString();
                    Label2.Text = dr[1].ToString();
                    user_id = dr[2].ToString();
                    user_access_id= Convert.ToInt32(user_id);

        }

           //  user_access_id= myReader[2].ToString();
           

        myReader = cmd.ExecuteReader();
        String username = userTxt.Text;

        if (dt.Rows.Count > 0)
        {
           // Response.Write(username );


            if (user_access_id == 1)
            {
                Response.Redirect("Admin/AdminMenu.aspx");
            }
            else
            {
                Response.Redirect("Default.aspx");
            }

        }
        else
        {
            Label1.Text = "Incorrect password";
            userTxt.Text = "";
            passTxt.Text = "";
        }

    }


}