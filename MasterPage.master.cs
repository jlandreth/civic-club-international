using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;


public partial class MasterPageOne : System.Web.UI.MasterPage
{
   /***
    * 
    * To create a new page
    * 1. Right Click on MasterPage
    * 2. In Popup click on Add Content Page
    * 3. It will name it Defaul(Some nember) 
    *   ex. Default2.aspx since there isn't one already named that
    * 4. Right click on page just created and select rename
    * 5. Rename page but be sure to put .aspx at the end or it want run
    * 6. Don't add a HTML <body> tage just place all code in between the
    *    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    *     <!--Insert HTML here-->
    *    </asp:Content>
    *    tags
    *    
    ***/
    protected void Page_Load(object sender, EventArgs e)
    {
        string chapter_name = "Chapter Name";
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CivicClubInternationalConnectionString"].ConnectionString))
        {
            string sql_select = "SELECT CHAPTER_NAME FROM CHAPTER_INFO";
            using (SqlCommand cmd = new SqlCommand(sql_select, conn))
            {
                cmd.Parameters.AddWithValue("@searchString", chapter_name);
                conn.Open();
                chapter_label_name.Text = cmd.ExecuteScalar().ToString();
            }
        }
    }


    protected void NavigationMenu_MenuItemClick(object sender, MenuEventArgs e)
    {

    }
}

