﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UpdateClubMember.aspx.cs" Inherits="UpdateClubMember" %>
<%@ MasterType VirtualPath="~/MasterPage.master" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" Visible="true" >
    <h1><asp:Label ID="Label1" runat="server" Text="Update Member" ></asp:Label></h1>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server" Height="19px" Width="213px" DataSourceID="SqlDataSource4" AutoPostBack="True" DataTextField="Member_name" DataValueField="member_id">
        </asp:DropDownList>
   <br />
        <asp:Label ID="Label2" runat="server" Text="Search by "></asp:Label>
        <asp:TextBox ID="EmpSearch" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Submit" />
    </p> 
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CivicClubInternationalConnectionString %>" DeleteCommand="DELETE FROM [MEMBERS] WHERE [MEMBER_ID] = @MEMBER_ID" InsertCommand="INSERT INTO [MEMBERS] ([MEMBER_COMPANY_NAME], [MEMBER_FIRST_NAME], [MEMBER_LAST_NAME], [MEMBER_ADDRESS_LINE1], [MEMBER_ADDRESS_LINE2], [MEMBER_CITY], [MEMBER_STATE], [MEMBER_COUNTRY], [MEMBER_ZIP], [MEMBER_PHONE_NUMBER], [MEMBER_EMAIL], [MEMBER_CATEGORY_ID], [MEMBER_STATUS_Id], [MEMBER_BEGIN_DATE], [CHAPTER_ID]) VALUES (@MEMBER_COMPANY_NAME, @MEMBER_FIRST_NAME, @MEMBER_LAST_NAME, @MEMBER_ADDRESS_LINE1, @MEMBER_ADDRESS_LINE2, @MEMBER_CITY, @MEMBER_STATE, @MEMBER_COUNTRY, @MEMBER_ZIP, @MEMBER_PHONE_NUMBER, @MEMBER_EMAIL, @MEMBER_CATEGORY_ID, @MEMBER_STATUS_Id, @MEMBER_BEGIN_DATE, @CHAPTER_ID)" SelectCommand="SELECT * FROM [MEMBERS]" UpdateCommand="UPDATE [MEMBERS] SET [MEMBER_COMPANY_NAME] = @MEMBER_COMPANY_NAME, [MEMBER_FIRST_NAME] = @MEMBER_FIRST_NAME, [MEMBER_LAST_NAME] = @MEMBER_LAST_NAME, [MEMBER_ADDRESS_LINE1] = @MEMBER_ADDRESS_LINE1, [MEMBER_ADDRESS_LINE2] = @MEMBER_ADDRESS_LINE2, [MEMBER_CITY] = @MEMBER_CITY, [MEMBER_STATE] = @MEMBER_STATE, [MEMBER_COUNTRY] = @MEMBER_COUNTRY, [MEMBER_ZIP] = @MEMBER_ZIP, [MEMBER_PHONE_NUMBER] = @MEMBER_PHONE_NUMBER, [MEMBER_EMAIL] = @MEMBER_EMAIL, [MEMBER_CATEGORY_ID] = @MEMBER_CATEGORY_ID, [MEMBER_STATUS_Id] = @MEMBER_STATUS_Id, [MEMBER_BEGIN_DATE] = @MEMBER_BEGIN_DATE, [CHAPTER_ID] = @CHAPTER_ID WHERE [MEMBER_ID] = @MEMBER_ID">
        <DeleteParameters>
            <asp:Parameter Name="MEMBER_ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MEMBER_COMPANY_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_FIRST_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_LAST_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_ADDRESS_LINE1" Type="String" />
            <asp:Parameter Name="MEMBER_ADDRESS_LINE2" Type="String" />
            <asp:Parameter Name="MEMBER_CITY" Type="String" />
            <asp:Parameter Name="MEMBER_STATE" Type="String" />
            <asp:Parameter Name="MEMBER_COUNTRY" Type="String" />
            <asp:Parameter Name="MEMBER_ZIP" Type="String" />
            <asp:Parameter Name="MEMBER_PHONE_NUMBER" Type="String" />
            <asp:Parameter Name="MEMBER_EMAIL" Type="String" />
            <asp:Parameter Name="MEMBER_CATEGORY_ID" Type="Int32" />
            <asp:Parameter Name="MEMBER_STATUS_Id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="MEMBER_BEGIN_DATE" />
            <asp:Parameter Name="CHAPTER_ID" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="MEMBER_COMPANY_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_FIRST_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_LAST_NAME" Type="String" />
            <asp:Parameter Name="MEMBER_ADDRESS_LINE1" Type="String" />
            <asp:Parameter Name="MEMBER_ADDRESS_LINE2" Type="String" />
            <asp:Parameter Name="MEMBER_CITY" Type="String" />
            <asp:Parameter Name="MEMBER_STATE" Type="String" />
            <asp:Parameter Name="MEMBER_COUNTRY" Type="String" />
            <asp:Parameter Name="MEMBER_ZIP" Type="String" />
            <asp:Parameter Name="MEMBER_PHONE_NUMBER" Type="String" />
            <asp:Parameter Name="MEMBER_EMAIL" Type="String" />
            <asp:Parameter Name="MEMBER_CATEGORY_ID" Type="Int32" />
            <asp:Parameter Name="MEMBER_STATUS_Id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="MEMBER_BEGIN_DATE" />
            <asp:Parameter Name="CHAPTER_ID" Type="Int32" />
            <asp:Parameter Name="MEMBER_ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CivicClubInternationalConnectionString %>" SelectCommand="select member_id,member_last_name+', '+ member_first_name as Member_name from members "></asp:SqlDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="MEMBER_ID" DataSourceID="SqlDataSource3" Height="50px"  Width="95%" style="margin-left:30px;" AllowPaging="True">
        <Fields>
            <asp:BoundField DataField="MEMBER_ID" HeaderText="Member ID" InsertVisible="False" ReadOnly="True" SortExpression="MEMBER_ID" />
            <asp:BoundField DataField="MEMBER_COMPANY_NAME" HeaderText="Company Name" SortExpression="MEMBER_COMPANY_NAME" />
            <asp:BoundField DataField="MEMBER_FIRST_NAME" HeaderText="First Name" SortExpression="MEMBER_FIRST_NAME" />
            <asp:BoundField DataField="MEMBER_LAST_NAME" HeaderText="Last Name" SortExpression="MEMBER_LAST_NAME" />
            <asp:BoundField DataField="MEMBER_ADDRESS_LINE1" HeaderText="Address" SortExpression="MEMBER_ADDRESS_LINE1" />
            <asp:BoundField DataField="MEMBER_ADDRESS_LINE2" HeaderText="Address" SortExpression="MEMBER_ADDRESS_LINE2" />
            <asp:BoundField DataField="MEMBER_CITY" HeaderText="City" SortExpression="MEMBER_CITY" />
            <asp:BoundField DataField="MEMBER_STATE" HeaderText="State" SortExpression="MEMBER_STATE" />
            <asp:BoundField DataField="MEMBER_COUNTRY" HeaderText="Country" SortExpression="MEMBER_COUNTRY" />
            <asp:BoundField DataField="MEMBER_ZIP" HeaderText="Zipcode" SortExpression="MEMBER_ZIP" />
            <asp:BoundField DataField="MEMBER_PHONE_NUMBER" HeaderText="Phone Number" SortExpression="MEMBER_PHONE_NUMBER" />
            <asp:BoundField DataField="MEMBER_EMAIL" HeaderText="Email" SortExpression="MEMBER_EMAIL" />
            <asp:BoundField DataField="MEMBER_CATEGORY_ID" HeaderText="Member Category Id" SortExpression="MEMBER_CATEGORY_ID" />
            <asp:BoundField DataField="MEMBER_STATUS_Id" HeaderText="Member Status Id" SortExpression="MEMBER_STATUS_Id" />
            <asp:BoundField DataField="MEMBER_BEGIN_DATE" HeaderText="Member Begin Date" SortExpression="MEMBER_BEGIN_DATE" />

            
            <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />

            
        </Fields>
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:CivicClubInternationalConnectionString %>" SelectCommand="select member_ID
from members
where member_id = @member_id">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="member_id" PropertyName="SelectedValue" />
        </SelectParameters>
</asp:SqlDataSource>
    <br />


</asp:Content>

